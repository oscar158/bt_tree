# bt_tree

#### 项目介绍
ROS行为树实现（Python），其基础实现参考pi_trees（ https://github.com/pirobot/pi_trees ），更着重于子树变量维护与行为独立的模板（templates），以及用于绘制行为树视图的visio模具。


#### 使用说明

1. https://www.cnblogs.com/Ezekiel/p/9915262.html
