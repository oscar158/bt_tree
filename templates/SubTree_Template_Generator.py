#!/usr/bin/env python


import os, sys


if __name__ == "__main__":
    base_path = os.path.split(os.path.realpath(__file__))[0]

    for index in range(1, len(sys.argv)):
        with open((base_path + "/SubTree/SubTree_Temp.py"), "r") as i:
            template = i.read()
            target = template.replace("Temp", sys.argv[index])
            with open ((base_path + "/SubTree_Gen/SubTree_%s.py" % sys.argv[index]), "w") as o:
                o.write(target)

    for index in range(1, len(sys.argv)):
        with open((base_path + "/SubTree/SubTree_Temp_Test.py"), "r") as i:
            template = i.read()
            target = template.replace("Temp", sys.argv[index])
            with open ((base_path + "/SubTree_Gen/SubTree_%s_Test.py" % sys.argv[index]), "w") as o:
                o.write(target)
