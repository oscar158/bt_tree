#!/usr/bin/env python


import threading

import rospy

from SubTree_Path import *


NodeStatusMaps = {
    NodeStatus.SUCCESS: "SUCCESS",
    NodeStatus.RUNNING: "RUNNING",
    NodeStatus.FAILURE: "FAILURE",
}


ActionNode, timer = None, None
def ActionNodeCycleRun():
    ActionNode.status = ActionNode.run()
    rospy.loginfo(NodeStatusMaps[ActionNode.status])

    global timer
    timer = threading.Timer(1, ActionNodeCycleRun)
    timer.setDaemon(True)
    timer.start()


if __name__ == "__main__":
    rospy.init_node("subtree_Path_test")
    Path_init()

    _, _, ActionNode, _ = Path_get_SubTree("Path")
    ActionNodeCycleRun()

    rospy.spin()

